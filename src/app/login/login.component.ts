import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private readonly nick: string = 'nickname';
  nickname: string;

  constructor(
    private router: Router
  ) { }

  saveNickname(nickname: string) {
    if (nickname.length < 4 || nickname === null || nickname.match(/^ *$/) !== null) {
      return;
    }

    localStorage.setItem(this.nick, nickname);
    this.router.navigate(['/dashboard']);
  }

  getNickname() {
    this.nickname =  localStorage.getItem(this.nick);
  }

  deleteNickname() {
    localStorage.removeItem(this.nick);
  }

  ngOnInit() {
    this.getNickname();
  }

}
