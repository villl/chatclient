export class Message {
    roomId: number;
    message: string;
    username: string;
    id: string;
    createdOn: number;
}
