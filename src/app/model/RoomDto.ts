import {Room} from './Room';

export class RoomDto {
    rooms: Room[];
}
