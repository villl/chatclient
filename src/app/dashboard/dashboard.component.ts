import { Component, OnInit } from '@angular/core';
import { Room } from '../model/Room';
import { ChatMessageService } from '../services/chat-message.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  rooms: Room[];

  constructor(private chatMessageService: ChatMessageService) { }

  getRooms() {
    this.chatMessageService.getRooms().subscribe(roomDto => this.rooms = roomDto.rooms);
  }

  ngOnInit() {
    this.getRooms();
  }

}
