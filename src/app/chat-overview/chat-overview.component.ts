import { Component, OnInit } from '@angular/core';
import {ChatMessageService} from '../services/chat-message.service';
import { Message } from '../model/Message';
import {ActivatedRoute, Router} from '@angular/router';
import { MessagePostDto } from '../model/MessagePostDto';

@Component({
  selector: 'app-chat-overview',
  templateUrl: './chat-overview.component.html',
  styleUrls: ['./chat-overview.component.css']
})
export class ChatOverviewComponent implements OnInit {

  private readonly nicknameKey = 'nickname';
  messages: Message[];
  message = '';
  names: string[];
  roomId: string;
  nickname: string;
  private timer;

  constructor(
    private chatMessageService: ChatMessageService,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) { }

  getMessages() {
    if (this.roomId === null) {
      return;
    }

    this.chatMessageService.getMessages(this.roomId).subscribe(messageDto => this.messages = messageDto.messages);
  }

  sendMessage(text: string) {
    if (this.roomId === null || text === null || text.match(/^ *$/) !== null) {
      return;
    }

    this.message = '';
    const messagePostDto = new MessagePostDto(localStorage.getItem(this.nicknameKey), text);
    this.chatMessageService.sendMessage(this.roomId, messagePostDto).subscribe();
  }

  IsLocaluser(): boolean {
    return true;
  }

  private filterNames() {
    for (const message of this.messages) {
      for (const name of this.names) {
        console.log(message + '  ' + name);
        if (message.username !== name) {
           this.names.push(message.username);
        }
      }
    }

  }

  private timeout() {
    this.getMessages();
    this.timer = setTimeout(() => this.timeout(), 5000);
  }

  addEmoji($event) {
    this.message += ' ' + $event.emoji.native + ' ';
    console.log($event.emoji);
  }

  checkUsername(): boolean {
    return true;
    if ('username' === localStorage.getItem(this.nicknameKey)) {
      return true;
    }
    return false;
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  private getNickname(): void {
    this.nickname = localStorage.getItem(this.nicknameKey);
  }

  ngOnInit() {
    this.getNickname();
    this.activatedRoute.params.subscribe(params => {
      this.roomId = params['roomId'];
      console.log(this.roomId);
    });
    this.timeout();
  }

}
