import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, map, tap } from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { MessageDto } from '../model/MessageDto';
import {RoomDto} from '../model/RoomDto';
import { MessagePostDto } from '../model/MessagePostDto';

@Injectable({
  providedIn: 'root'
})
export class ChatMessageService {

  private readonly url = 'https://api.uai.urbec.org/chat/v1/rooms';

  private readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json; charset=utf-8',
      'Authorization': 'Bearer uai-624'
    })
  };

  constructor(private http: HttpClient) { }

  public getRooms(): Observable<RoomDto> {
    return this.http.get<RoomDto>(this.url, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  public getMessages(id: string): Observable<MessageDto> {
    return this.http.get<MessageDto>(this.url + '/' + id + '/messages', this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  sendMessage(id: string, messagePostDto: MessagePostDto): Observable<any> {
    const body = JSON.stringify(messagePostDto);
    return this.http.post(this.url + '/' + id + '/messages', body, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  // private handleError<T> (operation = 'operation', result?: T) {
  //   return (error: any): Observable<T> => {
  //     // TODO: send the error to remote logging infrastructure
  //     console.error(error); // log to console instead
  //     // TODO: better job of transforming error for user consumption
  //     this.log(`${operation} failed: ${error.message}`);
  //     // Let the app keep running by returning an empty result.
  //     return of(result as T);
  //   };
  // }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
